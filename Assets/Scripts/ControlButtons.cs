﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlButtons : MonoBehaviour {

	public Button JumpButton;
	public Button DashButton;

	void Awake () {

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//iEnumerator to recover a disabled button
	IEnumerator RecoverJumpButton(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		JumpButton.interactable= true;
	}

	//iEnumerator to recover a disabled button
	IEnumerator RecoverDashButton(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		DashButton.interactable = true;
	}

	//metodo publico que capa un boton
	public void DisableJump(float waitTime) {
		JumpButton.interactable = false;
		StartCoroutine("RecoverJumpButton",waitTime);
	}

	//metodo publico que capa otro boton
	public void DisableDash(float waitTime) {
		DashButton.interactable = false;
		StartCoroutine("RecoverDashButton",waitTime);
	}

}
