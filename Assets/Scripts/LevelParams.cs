﻿/*
	parámetros y operaciones generales del nivel.

*/


using UnityEngine;
using System.Collections;

public class LevelParams : MonoBehaviour {

	[Range(0, PlayerData.MAX_VIDAS)]
	public int vidasIniciales 			= 0; // Si es cero es ignorado.
	
	[Range(0.1f, 10.0f)]
	public float velocidadScroll = 1F; // velocidad del juego según el nivel.
	
	
	private bool paused = false;
	
	void Start () {
		if ( vidasIniciales < 0 )
			PlayerData.Vidas = vidasIniciales;
			
		
	}
}
