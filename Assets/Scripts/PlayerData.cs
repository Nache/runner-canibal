﻿/*
	Controla los datos del player

*/


using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour {

	public bool resetearVidas 		= true; // si es 'true', las vidas se pondrán al máximo al inicio de la escena.
	public bool resetearScore		= true; // si es 'true', resetea el score a 0 al inicio de la escena.

	public const int MAX_VIDAS 	= 3; // constantes para la cantidad máxima de vidas posible.
	
	private static int vidas 		= 3; // vidas actuales. por defecto 3
	private static int score 		= 0; // puntuación actual.
	private static int totalScore	= 0; // no sé si es necesario guardar un registro de toda la puntuación acumulada.
	
	
	
	// ****************************** PROPIEDADES *******************************
	public static int Vidas {
		get { return vidas; }
		set { vidas = Mathf.Clamp( value, 0, MAX_VIDAS ); }
	}
	
	public static int Score {
		get { return score; }
		set {
			score += value;
			totalScore += value;
		}
	}
	
	public static int TotalScore {
		get { return totalScore; }
	}

	

	// ****************************** MÉTODOS **********************************
	void Awake ()
	{
		if ( resetearVidas )
			vidas = MAX_VIDAS;
		else if ( vidas < 1 )
			vidas = 1; // si no se resetea las vidas, se asegura que al menos tenga una al inicio de la escena.
		
		if ( resetearScore )
			score = 0;
	}

}
