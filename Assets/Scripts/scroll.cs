﻿using UnityEngine;
using System.Collections;

public class scroll : MonoBehaviour {

	public float AreaLength;

	[Range(0.1f,10f)]
	public float SpeedFactor = 1f;
	
	private Vector3 defaultPos,tempPos;

	void Awake() {
		defaultPos = transform.position;
		tempPos = new Vector3(defaultPos.x - AreaLength ,defaultPos.y,defaultPos.z);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < tempPos.x) {
			transform.position = defaultPos;
		} else {
			transform.Translate(new Vector3(-1f * SpeedFactor * Time.deltaTime,0,0));
		}
	}
}
